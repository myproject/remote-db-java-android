package fr.nuroz.remotedb.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import fr.nuroz.remotedb.R;
import fr.nuroz.remotedb.composant.BddListAdapter;
import fr.nuroz.remotedb.databinding.FragmentAccueilBinding;

public class AccueilFragment extends Fragment {
    AccueilViewModel accueilViewModel;

    public AccueilFragment() {
        super(R.layout.fragment_accueil);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentAccueilBinding binding = FragmentAccueilBinding.inflate(inflater, container, false);

        accueilViewModel = new ViewModelProvider(this).get(AccueilViewModel.class);
        accueilViewModel.init(requireContext());
        binding.setAccueilViewModel(accueilViewModel);
        binding.setLifecycleOwner(this);

        accueilViewModel.getDatabases().observe(getViewLifecycleOwner(), databases -> {
            binding.bddListView.setAdapter(new BddListAdapter(requireContext(), databases, getViewLifecycleOwner()));
        });

        binding.addBddButton.setOnClickListener(v -> {
            NavDirections nav = AccueilFragmentDirections.goToCreateDatabaseFragment();
            Navigation.findNavController(v).navigate(nav);
        });

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        // toolbar configuration
        accueilViewModel.initToolbar();
    }
}