package fr.nuroz.remotedb.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.stream.Collectors;

import fr.nuroz.remotedb.R;
import fr.nuroz.remotedb.composant.Utils;
import fr.nuroz.remotedb.databinding.FragmentCreateDatabaseBinding;
import fr.nuroz.remotedb.entity.db.DatabaseDbEntity;
import fr.nuroz.remotedb.entity.db.TypeDatabaseDbEntity;

public class CreateDatabaseFragment extends Fragment {
    private CreateDatabaseViewModel createDatabaseViewModel;

    public CreateDatabaseFragment() {
        super(R.layout.fragment_create_database);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentCreateDatabaseBinding binding = FragmentCreateDatabaseBinding.inflate(inflater, container, false);

        createDatabaseViewModel = new ViewModelProvider(this).get(CreateDatabaseViewModel.class);
        createDatabaseViewModel.init(requireContext());
        binding.setCreateDatabaseViewModel(createDatabaseViewModel);
        binding.setLifecycleOwner(this);

        createDatabaseViewModel.getTypeDatabaseDbEntityLiveData().observe(getViewLifecycleOwner(), databasesType -> {
            List<String> databasesTypeString = databasesType.stream()
                            .map(TypeDatabaseDbEntity::getLibelle)
                            .collect(Collectors.toList());

            ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), R.layout.dropdown_item, databasesTypeString);
            adapter.setDropDownViewResource(R.layout.dropdown_item);
            binding.databaseTypeSpinner.setAdapter(adapter);
        });

        binding.testConnectionButton.setOnClickListener(v -> {
            LiveData<Boolean> isFillLiveData = createDatabaseViewModel.formIsFill();
            LiveData<DatabaseDbEntity> newDatabase = createDatabaseViewModel.valueToObject();

            isFillLiveData.observe(getViewLifecycleOwner(), isFill -> {
                newDatabase.observe(getViewLifecycleOwner(), databaseDb -> {
                        LiveData<Pair<Boolean, String>> isConnectedLiveData = createDatabaseViewModel.testConnexion(databaseDb);

                        isConnectedLiveData.observe(getViewLifecycleOwner(), isConnected -> {
                            Snackbar snackbar;

                            if(isFill) {

                                if(isConnected.first) {
                                    snackbar = Utils.createSnackbar(requireView(), requireContext(), requireContext().getText(R.string.succesConnexionSnackBar),
                                            R.color.white, R.color.green);
                                } else {
                                    snackbar = Utils.createSnackbar(requireView(), requireContext(),
                                            String.format(requireContext().getString(R.string.failConnexionSnackBar), isConnected.second),
                                            R.color.white, R.color.red);
                                }

                            } else {
                                snackbar = Utils.createSnackbar(requireView(), requireContext(),
                                        requireContext().getString(R.string.formNotFill), R.color.white, R.color.red);
                            }
                            snackbar.show();

                            isConnectedLiveData.removeObservers(getViewLifecycleOwner());
                        });

                    newDatabase.removeObservers(getViewLifecycleOwner());
                });

                isFillLiveData.removeObservers(getViewLifecycleOwner());
            });
        });

        binding.saveButton.setOnClickListener(v -> {
            LiveData<DatabaseDbEntity> newDatabase = createDatabaseViewModel.valueToObject();

            newDatabase.observe(getViewLifecycleOwner(), database -> {
                newDatabase.removeObservers(getViewLifecycleOwner());

                if(createDatabaseViewModel.saveNewDatabase(database)) {
                    requireActivity().onBackPressed();
                } else {
                    Utils.createSnackbar(requireView(), requireContext(), requireContext().getString(R.string.tryToConnectError),
                            R.color.white, R.color.red).show();
                }
            });
        });

        TextWatcher resetIsConnectedTextWatcher = new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                createDatabaseViewModel.resetIsConnected();
            }
        };

        binding.hostEditText.addTextChangedListener(resetIsConnectedTextWatcher);
        binding.databaseNameEditText.addTextChangedListener(resetIsConnectedTextWatcher);
        binding.usernameEditText.addTextChangedListener(resetIsConnectedTextWatcher);
        binding.passwordEditText.addTextChangedListener(resetIsConnectedTextWatcher);


        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        // toolbar configuration
        createDatabaseViewModel.initToolbar();
    }
}
