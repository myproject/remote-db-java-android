package fr.nuroz.remotedb.fragment;

import android.content.Context;
import android.util.Pair;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import fr.nuroz.remotedb.Application;
import fr.nuroz.remotedb.R;
import fr.nuroz.remotedb.TypeDatabaseEnum;
import fr.nuroz.remotedb.controller.MysqlCtrl;
import fr.nuroz.remotedb.controller.PostgresCtrl;
import fr.nuroz.remotedb.dao.DatabaseDao;
import fr.nuroz.remotedb.dao.TypeDatabaseDao;
import fr.nuroz.remotedb.datastore.DataStoreApp;
import fr.nuroz.remotedb.datastore.KeyDatastoreEnum;
import fr.nuroz.remotedb.entity.db.DatabaseDbEntity;
import fr.nuroz.remotedb.entity.db.TypeDatabaseDbEntity;

@AndroidEntryPoint
public class CreateDatabaseViewModel extends ViewModel {
    private Context context;

    private final TypeDatabaseDao typeDatabaseDao;
    private final DatabaseDao databaseDao;
    private final MysqlCtrl mysqlCtrl;
    private final PostgresCtrl postgresCtrl;

    private MutableLiveData<String> hostNameLiveData;
    private MutableLiveData<String> databaseNameLiveData;
    private MutableLiveData<String> usernameLiveData;
    private MutableLiveData<String> passwordLiveData;
    private MutableLiveData<Integer> databaseTypePositionLiveData;
    private LiveData<List<TypeDatabaseDbEntity>> typeDatabaseDbEntityLiveData;
    private MutableLiveData<Boolean> isConnectedLiveData;

    @Inject
    public CreateDatabaseViewModel() {
        typeDatabaseDao = Application.databaseApp.typeDatabaseDao();
        databaseDao = Application.databaseApp.databaseDao();
        mysqlCtrl = new MysqlCtrl();
        postgresCtrl = new PostgresCtrl();
    }

    public void init(Context context) {
        this.context = context;

        hostNameLiveData = new MutableLiveData<>("");
        databaseNameLiveData = new MutableLiveData<>("");
        usernameLiveData = new MutableLiveData<>("");
        passwordLiveData = new MutableLiveData<>("");
        databaseTypePositionLiveData = new MutableLiveData<>(0);
        isConnectedLiveData = new MutableLiveData<>(false);
        typeDatabaseDbEntityLiveData = LiveDataReactiveStreams.fromPublisher(typeDatabaseDao.getAll());
    }

    public void initToolbar() {
        DataStoreApp.setValue(KeyDatastoreEnum.SHOW_BACK_BUTTON, true);
        DataStoreApp.setValue(KeyDatastoreEnum.TOOL_BAR_TITLE, context.getString(R.string.createDatabaseFragment));
    }

    public LiveData<List<TypeDatabaseDbEntity>> getTypeDatabaseDbEntityLiveData() {
        return typeDatabaseDbEntityLiveData;
    }

    public MutableLiveData<String> getHostNameLiveData() {
        return hostNameLiveData;
    }

    public MutableLiveData<String> getDatabaseNameLiveData() {
        return databaseNameLiveData;
    }

    public MutableLiveData<String> getUsernameLiveData() {
        return usernameLiveData;
    }

    public MutableLiveData<String> getPasswordLiveData() {
        return passwordLiveData;
    }

    public MutableLiveData<Integer> getDatabaseTypePositionLiveData() {
        return databaseTypePositionLiveData;
    }

    public LiveData<Boolean> formIsFill() {
        return Transformations.switchMap(hostNameLiveData, hostname ->
                Transformations.switchMap(databaseNameLiveData, databaseName ->
                 Transformations.switchMap(usernameLiveData, username ->
                  Transformations.map(passwordLiveData, password ->
                  {
                        Pattern patternIp = Pattern.compile("(([01]?[0-9]{1,2}|2[0-4][0-9]|25[0-5])\\.){3}([01]?[0-9]{1,2}|2[0-4][0-9]|25[0-5])");
                        Pattern patternHostname = Pattern.compile("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$");

                        Matcher matcherIp = patternIp.matcher(hostname);
                        Matcher matcherHostname = patternHostname.matcher(hostname);

                        return (matcherIp.matches() || matcherHostname.matches()) && !databaseName.trim().isEmpty()
                                && !username.trim().isEmpty() && !password.trim().isEmpty();
                  })
                 )
                )
        );
    }

    public LiveData<DatabaseDbEntity> valueToObject() {
        return Transformations.switchMap(typeDatabaseDbEntityLiveData, typeDatabases ->
                Transformations.switchMap(databaseTypePositionLiveData, position ->
                 Transformations.switchMap(hostNameLiveData, hostname ->
                  Transformations.switchMap(databaseNameLiveData, databaseName ->
                   Transformations.switchMap(usernameLiveData, username ->
                    Transformations.map(passwordLiveData, password ->
                            new DatabaseDbEntity(databaseName, hostname, username, password, typeDatabases.get(position).getId())
                    )
                   )
                  )
                 )
                )
        );
    }

    public LiveData<Pair<Boolean, String>> testConnexion(DatabaseDbEntity database) {
        TypeDatabaseDbEntity typeDatabase = typeDatabaseDao.findOne(database.getType()).blockingFirst();
        LiveData<Pair<Boolean, String>> isConnected;

        if(typeDatabase.getLibelle().equals(TypeDatabaseEnum.MYSQL.getValue())) {
            isConnected = new MutableLiveData<>(mysqlCtrl.testConnexion(database, context));
        } else {
            isConnected = new MutableLiveData<>(postgresCtrl.testConnexion(database, context));
        }

        if(Objects.requireNonNull(isConnected.getValue()).first) {
            isConnectedLiveData.postValue(true);
        } else {
            isConnectedLiveData.postValue(false);
        }

        return isConnected;
    }

    public void resetIsConnected() {
        isConnectedLiveData.postValue(false);
    }

    public boolean saveNewDatabase(DatabaseDbEntity database) {
        if(Boolean.TRUE.equals(isConnectedLiveData.getValue()))
            databaseDao.insert(database);

        return Boolean.TRUE.equals(isConnectedLiveData.getValue());
    }
}
