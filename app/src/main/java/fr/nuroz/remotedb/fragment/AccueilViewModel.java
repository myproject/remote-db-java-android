package fr.nuroz.remotedb.fragment;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import fr.nuroz.remotedb.Application;
import fr.nuroz.remotedb.R;
import fr.nuroz.remotedb.dao.DatabaseDao;
import fr.nuroz.remotedb.datastore.DataStoreApp;
import fr.nuroz.remotedb.datastore.KeyDatastoreEnum;
import fr.nuroz.remotedb.entity.db.DatabaseDbEntity;
import fr.nuroz.remotedb.entity.db.DatabaseFullDbEntity;

@AndroidEntryPoint
public class AccueilViewModel extends ViewModel  {
    private DatabaseDao databaseDao;

    private Context context;

    private LiveData<List<DatabaseFullDbEntity>> databases;

    @Inject
    public AccueilViewModel() {
        databaseDao = Application.databaseApp.databaseDao();
    }

    public void init(Context context) {
        this.context = context;
        databases = LiveDataReactiveStreams.fromPublisher(databaseDao.findAllFull());
    }

    public LiveData<List<DatabaseFullDbEntity>> getDatabases() {
        return databases;
    }

    public void createDatabase(DatabaseDbEntity newDatabase) {
        databaseDao.insert(newDatabase);
    }

    public LiveData<Boolean> hasDatabase() {
        return Transformations.map(databases, database -> database.size() > 0);
    }

    public void initToolbar() {
        DataStoreApp.setValue(KeyDatastoreEnum.SHOW_BACK_BUTTON, false);
        DataStoreApp.setValue(KeyDatastoreEnum.TOOL_BAR_TITLE, context.getString(R.string.accueilFragment));
    }
}
