package fr.nuroz.remotedb.composant;

import android.content.Context;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

public class Utils {
    public static Snackbar createSnackbar(View view, Context context, String message, int textColor, int bgColor) {
        Snackbar snackbar = Snackbar.make(view, message,Snackbar.LENGTH_SHORT)
                .setActionTextColor(textColor);

        snackbar.getView().setBackgroundColor(context.getColor(bgColor));

        return snackbar;
    }

    public static Snackbar createSnackbar(View view, Context context, CharSequence message, int textColor, int bgColor) {
        return createSnackbar(view, context, message.toString(), textColor, bgColor);
    }
}
