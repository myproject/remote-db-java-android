package fr.nuroz.remotedb.composant;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import java.util.List;

import fr.nuroz.remotedb.R;
import fr.nuroz.remotedb.TypeDatabaseEnum;
import fr.nuroz.remotedb.databinding.BddListItemBinding;
import fr.nuroz.remotedb.entity.db.DatabaseFullDbEntity;

public class BddListAdapter extends ArrayAdapter<DatabaseFullDbEntity> {

    LifecycleOwner lifecycleOwner;
    public BddListAdapter(Context context, List<DatabaseFullDbEntity> databases, LifecycleOwner lifecycleOwner) {
        super(context, R.layout.bdd_list_item, databases);
        this.lifecycleOwner = lifecycleOwner;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.bdd_list_item, parent, false);
        }

        BddListItemBinding binding = BddListItemBinding.bind(view);
        BddListAdapterViewModel bddListAdapterViewModel = new ViewModelProvider((ViewModelStoreOwner) getContext()).get(BddListAdapterViewModel.class);
        bddListAdapterViewModel.init();
        binding.setBddListAdapterViewModel(bddListAdapterViewModel);
        binding.setLifecycleOwner(lifecycleOwner);

        DatabaseFullDbEntity database = getItem(position);

        int icon = 0;
        if(TypeDatabaseEnum.MYSQL.getValue().equals(database.getTypeDatabase().getLibelle())) {
            icon = R.drawable.mysql_logo;
        } else if(TypeDatabaseEnum.POSTGRES.getValue().equals(database.getTypeDatabase().getLibelle())) {
            icon = R.drawable.postgres_logo;
        }

        binding.iconBddListItem.setImageResource(icon);
        binding.dbNameBddListItem.setText(database.getDatabase().getName());
        binding.dbHostBddListItem.setText(database.getDatabase().getHost());
        binding.dbUserBddListItem.setText(database.getDatabase().getUsername());
        binding.deleteBddListItem.setOnClickListener((v) -> {
            bddListAdapterViewModel.deleteDatabase(database.getDatabase());
        });


        return view;
    }
}
