package fr.nuroz.remotedb.composant;

import androidx.lifecycle.ViewModel;

import fr.nuroz.remotedb.Application;
import fr.nuroz.remotedb.dao.DatabaseDao;
import fr.nuroz.remotedb.entity.db.DatabaseDbEntity;

public class BddListAdapterViewModel extends ViewModel {

    private DatabaseDao databaseDao;

    public void init() {
        databaseDao = Application.databaseApp.databaseDao();
    }

    public void deleteDatabase(DatabaseDbEntity database) {
        databaseDao.delete(database.getId());
    }
}
