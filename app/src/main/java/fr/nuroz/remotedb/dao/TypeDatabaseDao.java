package fr.nuroz.remotedb.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import fr.nuroz.remotedb.entity.db.TypeDatabaseDbEntity;
import io.reactivex.rxjava3.core.Flowable;

@Dao
public interface TypeDatabaseDao {
    @Insert(entity = TypeDatabaseDbEntity.class, onConflict = OnConflictStrategy.REPLACE)
    void insert(TypeDatabaseDbEntity typeDatabase);

    @Query("SELECT * FROM type_database")
    Flowable<List<TypeDatabaseDbEntity>> getAll();

    @Query("SELECT * FROM type_database WHERE id = :id")
    Flowable<TypeDatabaseDbEntity> findOne(int id);
}
