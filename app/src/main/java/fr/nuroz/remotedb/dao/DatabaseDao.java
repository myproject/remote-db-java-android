package fr.nuroz.remotedb.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import fr.nuroz.remotedb.entity.db.DatabaseDbEntity;
import fr.nuroz.remotedb.entity.db.DatabaseFullDbEntity;
import io.reactivex.rxjava3.core.Flowable;

@Dao
public interface DatabaseDao {
    @Insert(entity = DatabaseDbEntity.class, onConflict = OnConflictStrategy.REPLACE)
    void insert(DatabaseDbEntity database);

    @Query("SELECT * FROM 'database'")
    Flowable<List<DatabaseDbEntity>> findAll();

    @Query("SELECT * FROM 'database'")
    Flowable<List<DatabaseFullDbEntity>> findAllFull();

    @Query("DELETE FROM 'database' WHERE id = :id")
    void delete(int id);
}
