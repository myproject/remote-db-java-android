package fr.nuroz.remotedb.entity.db;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "type_database")
public class TypeDatabaseDbEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String libelle;

    public TypeDatabaseDbEntity(int id, String libelle) {
        this.id = id;
        this.libelle = libelle;
    }

    @Ignore
    public TypeDatabaseDbEntity(String libelle) {
        this.libelle = libelle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
