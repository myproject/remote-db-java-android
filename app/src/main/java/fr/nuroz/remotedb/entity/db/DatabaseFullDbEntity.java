package fr.nuroz.remotedb.entity.db;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.Relation;

import java.lang.reflect.Type;

public class DatabaseFullDbEntity {
    @Embedded
    private DatabaseDbEntity database;

    @Relation(parentColumn = "type",
            entityColumn = "id",
            entity = TypeDatabaseDbEntity.class)
    private TypeDatabaseDbEntity typeDatabase;

    public DatabaseDbEntity getDatabase() {
        return database;
    }

    public void setDatabase(DatabaseDbEntity database) {
        this.database = database;
    }

    public TypeDatabaseDbEntity getTypeDatabase() {
        return typeDatabase;
    }

    public void setTypeDatabase(TypeDatabaseDbEntity typeDatabase) {
        this.typeDatabase = typeDatabase;
    }
}
