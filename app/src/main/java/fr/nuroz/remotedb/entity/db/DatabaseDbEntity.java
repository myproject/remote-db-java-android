package fr.nuroz.remotedb.entity.db;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "database")
public class DatabaseDbEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String host;
    private String username;
    private String password;
    private int type;

    public DatabaseDbEntity(int id, String name, String host, String username, String password, int type) {
        this.id = id;
        this.name = name;
        this.host = host;
        this.username = username;
        this.password = password;
        this.type = type;
    }

    @Ignore
    public DatabaseDbEntity(String name, String host, String username, String password, int type) {
        this.name = name;
        this.host = host;
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
