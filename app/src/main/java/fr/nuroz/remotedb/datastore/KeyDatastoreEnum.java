package fr.nuroz.remotedb.datastore;

public enum KeyDatastoreEnum {
    STORE_NAME(new MyKeyDatastore<>("DataStroreApp", "")),
    SHOW_BACK_BUTTON(new MyKeyDatastore<>("showBackButton", true)),
    TOOL_BAR_TITLE(new MyKeyDatastore<>("toolbarTitle", ""));

    private MyKeyDatastore key;
    KeyDatastoreEnum(MyKeyDatastore key) {
        this.key = key;
    }

    public MyKeyDatastore getKey() {
        return key;
    }
}
