package fr.nuroz.remotedb.datastore;

import java.lang.reflect.Type;

public class MyKeyDatastore<T> {
    private String key;
    private T defaultValue;

    public MyKeyDatastore(String key, T defaultValue) {
        this.key = key;
        this.defaultValue = defaultValue;
    }

    public String getKey() {
        return key;
    }

    public T getDefaultValue() {
        return defaultValue;
    }

    public boolean isGoodClaseType(Type classType) {
        return defaultValue.getClass().equals(classType);
    }
}
