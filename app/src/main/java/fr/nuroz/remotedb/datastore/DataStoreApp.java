package fr.nuroz.remotedb.datastore;

import static fr.nuroz.remotedb.datastore.KeyDatastoreEnum.STORE_NAME;

import android.content.Context;

import androidx.datastore.preferences.core.MutablePreferences;
import androidx.datastore.preferences.core.Preferences;
import androidx.datastore.preferences.core.PreferencesKeys;
import androidx.datastore.preferences.rxjava3.RxPreferenceDataStoreBuilder;
import androidx.datastore.rxjava3.RxDataStore;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Single;

public class DataStoreApp {
    private static RxDataStore<Preferences> dataStore;

    public static Completable init(Context context) {
        dataStore = new RxPreferenceDataStoreBuilder(context, KeyDatastoreEnum.STORE_NAME.getKey().getKey()).build();

        List<Completable> completables = new ArrayList<>();

        for(KeyDatastoreEnum key : KeyDatastoreEnum.values()) {
            if(key != STORE_NAME) {
                if(key.getKey().isGoodClaseType(String.class) && !DataStoreApp.isKeyExistString(key).blockingFirst()) {
                    completables.add(setValue(key, (String) key.getKey().getDefaultValue()));
                } else if(key.getKey().isGoodClaseType(Boolean.class) && !DataStoreApp.isKeyExistBoolean(key).blockingFirst()) {
                    completables.add(setValue(key, (boolean) key.getKey().getDefaultValue()));
                }
            }
        }

        return Completable.merge(completables);
    }

    public static  Flowable<Boolean> isKeyExistBoolean(KeyDatastoreEnum key) {
        return dataStore.data().map(preferences ->
                preferences.contains(PreferencesKeys.booleanKey(key.getKey().getKey())));
    }

    public static  Flowable<Boolean> isKeyExistString(KeyDatastoreEnum key) {
        return dataStore.data().map(preferences ->
                preferences.contains(PreferencesKeys.stringKey(key.getKey().getKey())));
    }

    public static Flowable<Boolean> getBooleanValue(KeyDatastoreEnum key) {
        return dataStore.data().map(prefs -> prefs.get(PreferencesKeys.booleanKey(key.getKey().getKey())));
    }

    public static Flowable<String> getStringValue(KeyDatastoreEnum key) {
        return dataStore.data().map(prefs -> prefs.get(PreferencesKeys.stringKey(key.getKey().getKey())));
    }

    public static Completable setValue(KeyDatastoreEnum key, boolean value) {
        return Completable.fromSingle(
            dataStore.updateDataAsync(prefs -> {
                MutablePreferences mutablePreferences =  prefs.toMutablePreferences();
                mutablePreferences.set(PreferencesKeys.booleanKey(key.getKey().getKey()), value);
                return Single.just(mutablePreferences);
            })
        );
    }

    public static Completable setValue(KeyDatastoreEnum key, String value) {
        return Completable.fromSingle(
                dataStore.updateDataAsync(prefs -> {
                    MutablePreferences mutablePreferences =  prefs.toMutablePreferences();
                    mutablePreferences.set(PreferencesKeys.stringKey(key.getKey().getKey()), value);
                    return Single.just(mutablePreferences);
                })
        );
    }
}
