package fr.nuroz.remotedb;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;

import java.util.Objects;

import fr.nuroz.remotedb.datastore.DataStoreApp;
import fr.nuroz.remotedb.datastore.KeyDatastoreEnum;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableObserver;
import io.reactivex.rxjava3.disposables.Disposable;

public class MainActivity extends AppCompatActivity {
    private LiveData<Boolean> showBackButton;
    private LiveData<String> toolbarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Completable initDatastoreCompletable = DataStoreApp.init(this);

        LifecycleOwner lifecycleOwner = this;

        initDatastoreCompletable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onComplete() {
                        showBackButton = LiveDataReactiveStreams.fromPublisher(
                                DataStoreApp.getBooleanValue(KeyDatastoreEnum.SHOW_BACK_BUTTON));
                        showBackButton.observe(lifecycleOwner, show ->
                                Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(show));

                        toolbarTitle = LiveDataReactiveStreams.fromPublisher(
                                DataStoreApp.getStringValue(KeyDatastoreEnum.TOOL_BAR_TITLE));
                        toolbarTitle.observe(lifecycleOwner, title -> Objects.requireNonNull(getSupportActionBar()).setTitle(title));
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        showBackButton.removeObservers(this);
    }
}