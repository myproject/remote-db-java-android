package fr.nuroz.remotedb.room;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import fr.nuroz.remotedb.dao.DatabaseDao;
import fr.nuroz.remotedb.dao.TypeDatabaseDao;
import fr.nuroz.remotedb.entity.db.DatabaseDbEntity;
import fr.nuroz.remotedb.entity.db.TypeDatabaseDbEntity;

@Database(entities = {DatabaseDbEntity.class, TypeDatabaseDbEntity.class}, version = 1, exportSchema = false)
public abstract class RoomDatabaseApp extends RoomDatabase {
    public abstract DatabaseDao databaseDao();
    public abstract TypeDatabaseDao typeDatabaseDao();
}
