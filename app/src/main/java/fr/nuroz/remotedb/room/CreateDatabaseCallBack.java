package fr.nuroz.remotedb.room;

import androidx.annotation.NonNull;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.Executors;

import fr.nuroz.remotedb.Application;
import fr.nuroz.remotedb.TypeDatabaseEnum;
import fr.nuroz.remotedb.dao.TypeDatabaseDao;
import fr.nuroz.remotedb.entity.db.TypeDatabaseDbEntity;

public class CreateDatabaseCallBack extends RoomDatabase.Callback {
    @Override
    public void onCreate(@NonNull SupportSQLiteDatabase db) {
        super.onCreate(db);
        Executors.newSingleThreadScheduledExecutor().execute(() -> {
            TypeDatabaseDao typeDatabaseDao = Application.databaseApp.typeDatabaseDao();
            typeDatabaseDao.insert(new TypeDatabaseDbEntity(TypeDatabaseEnum.MYSQL.getValue()));
            typeDatabaseDao.insert(new TypeDatabaseDbEntity(TypeDatabaseEnum.POSTGRES.getValue()));
        });
    }
}