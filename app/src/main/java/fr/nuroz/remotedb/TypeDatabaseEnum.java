package fr.nuroz.remotedb;

public enum TypeDatabaseEnum {
    MYSQL("MySQL"),
    POSTGRES("PostgreSQL");

    private String value;
    TypeDatabaseEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
