package fr.nuroz.remotedb.controller;

import android.content.Context;
import android.os.StrictMode;
import android.util.Pair;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import fr.nuroz.remotedb.R;
import fr.nuroz.remotedb.entity.db.DatabaseDbEntity;

public class PostgresCtrl extends DatabaseCtrl {

    @Override
    public Pair<Boolean, String> testConnexion(DatabaseDbEntity database, Context context) {
        String url = String.format("jdbc:postgresql://%s:5432/%s", database.getHost(), database.getName());

        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            Class.forName("org.postgresql.Driver");

            Connection connection = DriverManager.getConnection(url, database.getUsername(), database.getPassword());
            connection.close();
            return new Pair<>(true, "");
        } catch (SQLException e) {
            e.printStackTrace();
            return new Pair<>(false, e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return new Pair<>(false, context.getString(R.string.errorApplication));
        }
    }
}
