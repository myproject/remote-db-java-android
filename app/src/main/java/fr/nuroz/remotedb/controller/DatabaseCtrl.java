package fr.nuroz.remotedb.controller;

import android.content.Context;
import android.util.Pair;

import fr.nuroz.remotedb.entity.db.DatabaseDbEntity;

public abstract class DatabaseCtrl {
    public abstract Pair<Boolean, String> testConnexion(DatabaseDbEntity database, Context context);
}
