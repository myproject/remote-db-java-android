package fr.nuroz.remotedb;

import androidx.room.Room;

import com.facebook.flipper.BuildConfig;
import com.facebook.flipper.android.AndroidFlipperClient;
import com.facebook.flipper.android.utils.FlipperUtils;
import com.facebook.flipper.core.FlipperClient;
import com.facebook.flipper.plugins.databases.DatabasesFlipperPlugin;
import com.facebook.flipper.plugins.inspector.DescriptorMapping;
import com.facebook.flipper.plugins.inspector.InspectorFlipperPlugin;
import com.facebook.flipper.plugins.network.NetworkFlipperPlugin;
import com.facebook.soloader.SoLoader;

import dagger.hilt.android.HiltAndroidApp;
import fr.nuroz.remotedb.room.CreateDatabaseCallBack;
import fr.nuroz.remotedb.room.RoomDatabaseApp;

@HiltAndroidApp
public class Application extends android.app.Application {
    public static RoomDatabaseApp databaseApp;

    @Override
    public void onCreate() {
        super.onCreate();

        SoLoader.init(this, false);

        if (FlipperUtils.shouldEnableFlipper(this)) {
            FlipperClient client = AndroidFlipperClient.getInstance(this);
            client.addPlugin(new InspectorFlipperPlugin(this, DescriptorMapping.withDefaults()));
            client.addPlugin(new DatabasesFlipperPlugin(this));
            client.addPlugin(new NetworkFlipperPlugin());
            client.start();
        }

        databaseApp = Room.databaseBuilder(getApplicationContext(),
                        RoomDatabaseApp.class, "room.db")
                .addCallback(new CreateDatabaseCallBack())
                .allowMainThreadQueries()
                .build();

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
